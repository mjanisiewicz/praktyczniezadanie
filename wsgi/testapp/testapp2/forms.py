__author__ = 'Marek'
from django import forms

class TestForm(forms.Form):
    title = forms.CharField(min_length=5, max_length=100)
    content = forms.CharField(min_length=10, max_length=200, widget=forms.Textarea)

class CommentForm(forms.Form):
    content = forms.CharField(min_length=10, max_length=200, widget=forms.Textarea)

class LoginForm(forms.Form):
    login = forms.CharField(max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)

class CalcForm(forms.Form):
    wyrazenie = forms.CharField(max_length=50)