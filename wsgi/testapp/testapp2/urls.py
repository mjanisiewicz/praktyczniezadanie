from django.conf.urls import patterns, include, url
from django.conf import settings
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
import views
import os
PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))
urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.home, name='home'),
    url(r'add^$', views.add, name='add'),
    url(r'^staticfiles/(?P<path>.*)$', 'django.views.static.serve',{'document_root': PROJECT_PATH + '/static'}, name='staticfiles'),
    (r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
    url(r'^admin/', include(admin.site.urls)),

)

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()