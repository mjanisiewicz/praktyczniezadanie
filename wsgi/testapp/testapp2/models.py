__author__ = 'Marek'
from django.db import models

class TestEntry(models.Model):
    title = models.CharField(max_length=100)
    content = models.CharField(max_length=200)

class Comment(models.Model):
    content = models.CharField(max_length=100)
    article = models.ForeignKey(TestEntry,  null=True, blank=True)
    commid = models.ForeignKey('self', null=True, blank=True)

