__author__ = 'Marek'
# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.template import Template, Context
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from django.core.urlresolvers import reverse
from django.contrib import messages
from models import TestEntry, Comment
from forms import LoginForm
from django.contrib.auth import authenticate, login
import requests
import json
import os
dajaxice_autodiscover()
PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))
#test2
def home(request):
    #rejestracja serwera w bazie
    import couchdb

    try:
        url = 'http://194.29.175.241:5984/calc/_design/utils/_view/list_active'
        resp = requests.get(url=url)
        data = json.loads(resp.content)
        active = False
        for row in data['rows']:
            if row['value']['operator'] == '+' and row['value']['host'] == 'http://test2-pythonapps.rhcloud.com/':
                active = True

        if not active:
            couch = couchdb.Server('http://194.29.175.241:5984')
            db = couch['calc']
            doc = {'host': 'http://test2-pythonapps.rhcloud.com/', 'active': True, 'operator': '+', 'calculation': 'http://test2-pythonapps.rhcloud.com/add'}
            db.save(doc)
    except:
        pass
    return render(request, 'default_content.html')

def add(request) :
    if request.method == "POST" :
        uuid = request.POST['uuid']
        number1 = request.POST['number1']
        number2 = request.POST['number2']

    return HttpResponse(json.dumps(number1 + number2))
