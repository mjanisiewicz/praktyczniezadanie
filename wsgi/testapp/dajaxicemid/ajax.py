__author__ = 'Marek'
from django.utils import simplejson
from dajaxice.decorators import dajaxice_register
from tasks import add
@dajaxice_register
def sayhello(request):
    result = add.delay(4,4)
    result = result.get()
    return simplejson.dumps({'message':str(result)})

@dajaxice_register
def onp(request, data):
    text = data
    text = text.split(' ')
    stos = []
    try:
        for i in text:
            try:
                stos.append(int(i))
            except ValueError:
                if i == '+':
                    stos.append(stos.pop(len(stos) - 2) + stos.pop(len(stos) - 1))
                elif i == '-':
                    stos.append(stos.pop(len(stos) - 2) - stos.pop(len(stos) - 1))
                elif i == '*':
                    stos.append(stos.pop(len(stos) - 2) * stos.pop(len(stos) - 1))
                elif i == '/':
                    stos.append(stos.pop(len(stos) - 2) / stos.pop(len(stos) - 1))
    except IndexError:
        pass
    return simplejson.dumps({'message':str(stos)})