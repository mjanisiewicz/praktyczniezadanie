__author__ = 'Marek'
from celery import task
import json

@task()
def add(x, y):
    return x + y

@task()
def diff(x, y):
    return x - y

@task()
def onp(text):
    stos = text.split
    return json.dumps(stos)
